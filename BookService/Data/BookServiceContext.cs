﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookService.Models;

namespace BookService.Models
{
    public class BookServiceContext : DbContext
    {
        public BookServiceContext (DbContextOptions<BookServiceContext> options)
            : base(options)
        {
        }

        public DbSet<BookService.Models.Author> Author { get; set; }
        public DbSet<BookService.Models.Book> Book { get; set; }
        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Author>().HasData
        //        (
        //            new Author { Id = 1, Name = "Abinash Rana" }
        //        );
        //}
    }
}
